# Analizador de sentimientos en tweets

En este repositorio se encunetran los código utilizados en el trabajo de tesis titulado _Analisis de sentimientos en tweets financieros_. Este trabajo utiliza [notebooks de jupyter](https://es.wikipedia.org/wiki/Proyecto_Jupyter#Jupyter_Notebook).

## Instalación del ambiente de trabajo Python

Para ejecutar los códigos en este repositorio es necesario instalar [miniconda](https://docs.conda.io/en/latest/miniconda.html). Una vez instalado, se debe crear un ambiente python utilizando el archivo environment.yml del directorio util con el comando

```
conda env create -f environment.yml
```

Una vez creado el environment python se debe activar el environment y descargar los modelos de spaCy en inglés y español para el preprocesamiento de datos con los comandos.

```
activate tesis_jlrm
python -m spacy download es_core_news_sm
python -m spacy download en_core_web_sm
```

La descarga de los modelos spaCy se ejecuta únicamente una vez.

## Ejecución de los códigos

Para activar el ambiente visual de los notebooks de jupyter, ejecutar en la misma consola donde se tiene activo el ambiente.

```
jupyter notebooks
```

Esto abrirá un ambiente visual en el navegador predeterminado del sistema y se podrá ejecutar el código de este repositorio.